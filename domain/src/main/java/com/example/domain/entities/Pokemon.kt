package com.example.domain.entities

data class Pokemon(val id: Int?, val name: String?, val weight: Int?, val height: Int?, val sprites: Sprites?)

